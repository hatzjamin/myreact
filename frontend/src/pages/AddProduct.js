// modules
import {Button, Form, Row, Col, Container} from 'react-bootstrap';
import styled from 'styled-components';
import Navbar from '../components/AppNavbar';
import { Link } from 'react-router-dom';
import React, { useContext, useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


const AddProduct = () => {

    const [title, setTitle] = useState("");
    const [price, setPrice] = useState(0);
    const [description, setDescription] = useState("");
    const [images, setImages] = useState("");
    const [category, setCategory] = useState("");
    const [stock, setStock] = useState(0);
    const [isActive, setIsActive] = useState(false);

    const navigate = useNavigate();

    const {user} = useContext(UserContext);

    useEffect(() => {
        title !== "" && price !== null && description !== "" && images !== "" && category !== "" && stock !== null
        ? setIsActive(true)
        : setIsActive(false)

    }, [title, price, description, images, category, stock, isActive]);

    function addProduct(e){
        e.preventDefault();

 
        if(user.isAdmin){
            fetch(`${process.env.REACT_APP_API_URI}/products/add`, {
                method: "POST",
                headers: {
                    'Content-Type' : 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    title,
                    price,
                    description,
                    images,
                    category,
                    stock
                })
                
            }).then(response => response.json())
            .then(data =>{
                console.log(data);
                    if(data.myImage === false) {
                        Swal.fire({
                            title: "No Image Uploaded",
                            icon: "error",
                            text: "Pleas upload an image"
                        })

                    } else {
                        Swal.fire({
                            title: "Product Created",
                            icon: "success",
                            text: "Thank you for your patience"
                        })

                        navigate('/');
                    }
                })
        } else {
            Swal.fire({
                title: "Access Denied",
                icon: "success",
                text: "You are not authorized to do this type of action."
            })
        }
    }

    return (
        <>  
            <Container>
                
                    <title>Add Product</title>
                    <Form onSubmit={addProduct}>
                        <input
                            type="text"
                            placeholder="Name"
                            value={title}
                            onChange={event => setTitle(event.target.value)}
                            required/>
                        <input
                            type="number"
                            placeholder="Price"
                            value={price}
                            onChange={event => setPrice(event.target.value)}
                            required/>
                        <input
                            type="text"
                            placeholder="Description"
                            value={description}
                            onChange={event => setDescription(event.target.value)}
                            required/>
                        <input
                            placeholder="Image"
                            type="text"
                            value={images}
                            onChange={event => setImages(event.target.value)}
                            required/>
                        <input
                            placeholder="Category"
                            type="text"
                            value={category}
                            onChange={event => setCategory(event.target.value)}
                            required/>

                        <input
                            placeholder="Stock"
                            type="number"
                            value={stock}
                            onChange={event => setStock(event.target.value)}
                            required/>

                        <Button type="submit">Add Product</Button>
                       
                    </Form>

            </Container>
        </>
  )
}

export default AddProduct

