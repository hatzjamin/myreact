import { useEffect, useState } from "react"
// import Footer from "../components/Footer";
import AdminProducts from "../components/AdminProducts";
import { Link } from "react-router-dom";

const AdminDashboard = () => {
  const [products, setProducts] = useState([]);
  const [isUpdated, setIsUpdated] = useState(false);

  useEffect(() => {

    fetch(`${process.env.REACT_APP_API_URI}/products/allActiveProducts`, {
        headers: {
            'Content-Type' : 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
    })
    .then(response => response.json())
    .then(data => {
        console.log(data);

        setProducts(data.map(product => {
            return (<AdminProducts key={product._id} product={product} setIsUpdated={setIsUpdated}/>)
        }))
        setIsUpdated(false);
    })

  }, [isUpdated]);

  return (
    <Container>
        <Wrapper>
            <Title>All Products</Title>
            <Top>
                <TopButton as={Link} to="/" type="unfilled" style={{border: "2px solid #000", fontWeight: "500"}}>HOME</TopButton>

                <TopButton type="filled">History</TopButton>
            </Top>
            <Bottom>
                <Info>
                    {products}
                </Info>
            </Bottom>
        </Wrapper>
        <Footer />
    </Container>
        
  )
}

export default AdminDashboard