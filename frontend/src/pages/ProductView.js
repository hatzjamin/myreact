import {useState, useEffect} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap'
import {useParams, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2'
export default function ProductView(){
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const {productId} = useParams();

	const history = useNavigate();
	

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URI}/products/${productId}`)
		.then(response => response.json())
		.then(data => {
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId]);

	const buy = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/buy/${productId}`, {
			method: "POST",
			headers: {
				"Content-Type": 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire({
					title: "Successfully bought!",
					icon: "success",
					text: "Thank you!"
				})

				history("/products");
			}else{
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please Try Again!"
				})
				history("/");
			}
		})
	}

	return(
		<Container>
			<Row>
			<Col lg = {{span: 6, offset: 3}}>
				<Card>
					    <Card.Body className="text-center">
									<Card.Title>{name}</Card.Title>
									<Card.Subtitle>Description:</Card.Subtitle>
									<Card.Text>{description}</Card.Text>
									<Card.Subtitle>Price:</Card.Subtitle>
									<Card.Text>PhP {price}</Card.Text>
									<Card.Subtitle>Lorem ipsum</Card.Subtitle>
									<Card.Text>Lorem ipsum</Card.Text>
									<Button variant="primary" onClick = {()=> buy(productId)}>Add to Cart</Button>
								</Card.Body>	
					</Card>
			</Col>
			</Row>
		</Container>
		)
}