import React, { useEffect, useState } from 'react'
import { Button, Col, Container, Form, Row } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom';
// import UserContext from '../UserContext';
// import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

const Register = () => {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [userName, setUserName] = useState("");
    const [email, setEmail] = useState("");
    const [mobile, setMobile] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    const [isActive, setIsActive] = useState(false);

    const navigate = useNavigate();

    useEffect(() => {
        firstName !== "" && lastName !== "" && userName !== "" && email !== "" && mobile !== "" && password1 !== "" && password2 !== "" && password1 === password2
        ? setIsActive(true)
        : setIsActive(false)

    }, [firstName, userName, lastName, email, mobile, password1, password2]);

     const submitRegister = e => {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URI}/users/register`, {
            method: "POST",
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                username: userName,
                email: email,
                mobileNo: mobile,
                password1: password1,
                password2: password2,
            })

        }).then(response => response.json())
        .then(data =>{
            console.log(data);

            if(data.emailExists) {
                Swal.fire({
                    title: "Email Already Exists",
                    icon: "error",
                    text: "Please use another email"
                })

                setPassword1('');
                setPassword2('');
            } 

            else if (data.mobileLength === false) {
                Swal.fire({
                    title: "Invalid Mobile Number",
                    icon: "error",
                    text: "Mobile number must be at least 11 digits to be valid."
                })

                setPassword1('');
                setPassword2('');
            }

            else {
                Swal.fire({
                    title: "Registration Successful",
                    icon: "success",
                    text: "You may now login to our site!"
                })

                navigate('/login');
            }
        })
    }

    return (
        <Container>
            <Row className='w-100'>
                <Col className='col-md-4 col-8 offset-md-4 offset-2'>
                    <Form className='bg-secondary p-3' onSubmit={submitRegister}>
                        <Form.Group className="mb-3" controlId="firstName">
                            <Form.Label>First Name</Form.Label>
                            <Form.Control
                                value={firstName}
                                type="text"
                                placeholder="Enter first name"
                                required
                                onChange={e => setFirstName(e.target.value)}/>
                            <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="lastName">
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control
                                value={lastName}
                                type="text"
                                placeholder="Enter last name"
                                required
                                onChange={e => setLastName(e.target.value)}/>
                            <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="userName">
                            <Form.Label>Username</Form.Label>
                            <Form.Control
                                value={userName}
                                type="text"
                                placeholder="Enter username"
                                required
                                onChange={e => setUserName(e.target.value)}/>
                            <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="email">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                                value={email}
                                type="email"
                                placeholder="Enter email"
                                required
                                onChange={e => setEmail(e.target.value)}/>
                            <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="mobile">
                            <Form.Label>Mobile Number</Form.Label>
                            <Form.Control
                                value={mobile}
                                type="number"
                                placeholder="Enter mobile number"
                                required
                                onChange={e => setMobile(e.target.value)}/>
                            <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="passwordOne">
                            <Form.Label>Enter your desired password</Form.Label>
                            <Form.Control
                                value={password1}
                                type="password"
                                placeholder="Password"
                                required
                                onChange={e => setPassword1(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="passwordTwo">
                            <Form.Label>Verify password</Form.Label>
                            <Form.Control
                                value={password2}
                                type="password"
                                placeholder="Password"
                                required
                                onChange={e => setPassword2(e.target.value)}/>
                        </Form.Group>
                        
                        <Button variant="primary" type="submit" disabled={!isActive}>
                            Register
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
        
    )
}

export default Register
