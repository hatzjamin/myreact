const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
		{
			name : {
				type : String,
				required : [true, "name is required."]
			},
			username: {
				type : String,
				required : [true, "Username is required."],
				unique: true
			},
			email : {
				type : String,
				required : [true, "Email is required."],
				unique: true
			},
			password : {
				type : String,
				required : [true, "Password is required."]
			},
			cart: [{
				type: mongoose.Schema.Types.ObjectId,
				ref: 'Cart',
			  }],
			isAdmin : {
				type : Boolean,
				default : false
			}
		},
		{
			timestamps: true
		}
	)

module.exports = mongoose.model("User", userSchema);
