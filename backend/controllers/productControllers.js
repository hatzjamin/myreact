const Product = require("../models/Product");
const auth =require("../auth");

module.exports.addProduct = (request, response) =>{
		
		let token = request.headers.authorization;
		const userData = auth.decode(token);
		console.log(userData);

		let newProduct = new Product({
			title: request.body.title,
			description: request.body.description,
			images: request.body.images,
			category: request.body.category,
			price: request.body.price,
			stocks: request.body.stocks
		})

		if(userData.isAdmin){
			let myImage;
			if (images === ""){
				return response.status(400).send({myImage: false});
			}

			newProduct.save().then(result => {
				console.log(result);
				response.send(true);
			}).catch(error => {
				console.log(error);
				response.send(false);
			})
		}
		else{
			response.send("You do not have an Admin role!")
		}
}

// Retrieve all active products

module.exports.getAllActive = (request, response) =>{
	return Product.find({isActive: true})
	.then(result => {
		response.send(result);
	}).catch(err => {
		response.send(err);
	})
}

// Retrive all products 

module.exports.getAllProducts = (request, response) =>{
	const token = request.headers.authorization;
	const userData = auth.decode(token);

	if(!userData.isAdmin){
		return response.send("Sorry, you don't have access to this page!");
	}else{
		return Product.find({}).then(result => response.send(result))
		.catch(err => {
			console.log(err);
			response.send(err);
		})
	}
}


// retrieving a specific product

module.exports.getProduct = (request, response) => {
	const productId = request.params.productId;

	return Product.findById(productId).then(result =>{
		response.send(result);
	}).catch(err =>{
		response.send(err);
	})

}

// update a product

module.exports.updateProduct = (request, response) => {
	const token = request.headers.authorization;
	const userData = auth.decode(token);

	console.log(userData);

		let updatedProduct = {
			name: request.body.name,
			description: request.body.description,
			price: request.body.price,
			stocks: request.body.stocks
		}

	const productId = request.params.productId;

	if(userData.isAdmin){
		return Product.findByIdAndUpdate(productId, updatedProduct, {new:true}).then(result => {
			response.send(result)
		}).catch(err =>{
			response.send(err);
		})
	}else{
		return response.send("You don't have access to this page!");
	}
}

// archive a product

module.exports.archiveProduct = (request, response) => {
	const token = request.headers.authorization;
	const userData = auth.decode(token);

	let productId = request.params.productId;
	let updateIsActive = {
			isActive: request.body.isActive
		}
	

	if(userData.isAdmin){
		return Product.findByIdAndUpdate(productId, updateIsActive, {new:true}).then(result => {
			response.send(true)
		}).catch(error =>{
			response.send(false);
		})
	}
	else{
		return response.send("You don't have access to this page!");
	}

}

// unarchive a product

module.exports.unArchiveProduct = (request, response) => {
	const token = request.headers.authorization;
	const userData = auth.decode(token);

	let productId = request.params.productId;
	let updateIsActive = {
			isActive: request.body.isActive
		}
	

	if(userData.isAdmin){
		return Product.findByIdAndUpdate(productId, updateIsActive, {new:true}).then(result => {
			response.send(true)
		}).catch(error =>{
			response.send(false);
		})
	}
	else{
		return response.send("You don't have access to this page!");
	}

}