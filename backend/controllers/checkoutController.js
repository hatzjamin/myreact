// import modules
const User = require('../models/User');
const Cart = require('../models/Cart');
const Checkout = require('../models/Checkout');
const auth = require('../auth');

// checkout
const checkout = async (request, response) => {
    // get user payload
	const userData = auth.decode(request.headers.authorization);
	const cartItemId = request.params.cartItemId;

    if (!userData.isAdmin) {
        const cartItem = await Cart.findOne({_id: cartItemId})

        if(!cartItem) {
            return response.status(400).json({error: 'No items found.'})
        }

        let newCheckout = new Checkout(
            {
                cartItemId,
                userId: userData.id,
                productId: cartItem.productId,
                quantity: cartItem.quantity,
                amount: cartItem.amount
            }
        )

        await newCheckout.save();
        await Cart.findOneAndDelete({_id: cartItemId});

        // update user cart
        const user = await User.findById(userData.id);
		let items = user.cart;

		const updatedCart = items.filter(item =>{
			if(!item.equals(cartItem._id)) {
				return item;
			}
		})

		user.cart = updatedCart;
		await user.save();
		
		response.status(200).json(cartItem);
    }
}

module.exports = {
    checkout,
}
