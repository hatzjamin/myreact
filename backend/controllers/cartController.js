// import modules
const Product = require('../models/Product');
const User = require('../models/User');
const Cart = require('../models/Cart');
const auth = require('../auth');

// Add to cart
module.exports.addToCart = async (request, response) => {
	// get User payload
	const userData = auth.decode(request.headers.authorization);

	// get target Product details
	const productId = request.params.productId;

	const cart = await Cart.findOne({productId, userId: userData.id});
	if(!cart) {
		if (!userData.isAdmin) {
			const product = await Product.findById(productId);
			let newCart = new Cart(
				{
					userId: userData.id,
					productId,
					amount: product.price
				}
			);
	
			await newCart.save();
	
			// update user cart
			const user = await User.findById(userData.id);
			user.cart.push(newCart);
			await user.save();
	
			return response.send(user);
		} else {
			response.json({msg: "You are an admin. Only customers are allowed to purchase."})
		}
	} else {
		response.send("Cannot add the same item.")
	}
}

// Remove from cart
module.exports.removeFromCart = async (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const cartItemId = request.params.cartItemId;
	const cart = await Cart.find({id: userData.id})
	
	if(cart) {
		const cartItem = await Cart.findOneAndDelete({_id: cartItemId})
		if(!cartItem) {
			return response.status(400).json({error: 'No items found.'})
		}
		
		// update user cart
        const user = await User.findById(userData.id);
		let items = user.cart;

		const updatedCart = items.filter(item =>{
			if(!item.equals(cartItem._id)) {
				return item;
			}
		})

		user.cart = updatedCart;
		await user.save();
		
		response.status(200).json(cartItem);
	}
}
