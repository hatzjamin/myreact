const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth.js");

// Adding product
router.post("/add", auth.verify, productControllers.addProduct);

// All active products
router.get("/allActiveProducts", productControllers.getAllActive);

// // Retrieve all courses
// router.get("/allProducts", auth.verify, productControllers.getAllProducts);

// // Get Speficific product
// router.get("/:productId", productControllers.getProduct);

// // Update specific product
// router.put("/update/:productId", auth.verify, productControllers.updateProduct);

// // archive a product
// router.patch("/:productId/archive", auth.verify, productControllers.archiveProduct);

// // unarchive a product
// router.patch("/:productId/unarchive", auth.verify, productControllers.unArchiveProduct);


module.exports = router;